//CD-Pipeline-Template
//Version: 0.11
//Author: Steve Lohr (steve.lohr@deutschepost.de)
//###README########################################################################################################################################
//this file has to be placed directly in / (root) of your repository, check this wiki for further guidance (https://lcm.deutschepost.de/sf/go/wiki15560)
//use Jenkins job type multi-branch-pipeline
//shared resources must be locked before and freed after usage
//use milestones after manual steps within pipeline to close older builds, which haven't passed the milestone yet
//use as many prepared DSL-commands as possible (from Pipeline-Plugins or from CDlib https://lcm-scms2.deutschepost.de/gerrit/tf_cd_standardbibliothek)
//read the commentary within this template, mandatory steps are marked with ! and are not allowed to be removed, e. g. //!Change-Creation
//steps that can be performed in parallel should be run in parallel, e. g. Source Code Check, OSLC-Check and Checkmarx
//always check your pipeline with the declarative-linter (https://jenkins.io/doc/book/pipeline/development/) before you commit it to the repository
//#################################################################################################################################################

//define which version of the TF-CD-Library this pipeline is compatible to
@Library('TF_CD_LIB@master') _
pipeline {
    options {
        //enable timestamps in console output
        timestamps()
    }
    triggers {
        //polling SCM with cron syntax for new changes to trigger a build, default: every minute
        pollSCM('* * * * *')
    }
    // This configures the agent, where the pipeline will be executed. The default should be `none` so
    // that `Approval` stage does not block an executor on the master node.
    agent {
        node none
    }
    stages {
        stage('BUILD') {
            agent {
                node {
                    //if not specified otherwise, everything runs on the Jenkins master
                    label 'master'
                    //create a custom workspace for every build to avoid interference due to concurrent builds
                    customWorkspace "jobs/${env.JOB_NAME}/${env.BUILD_NUMBER}"
                }
            }
            steps {
                //TODO: artf434452
                script {
                    //load pipeline configuration from same path as Jenkinsfile, default is config.jenkins
                    //TODO: artf439099
                    config = load 'config.jenkins'
                    maven = new dhl.tools.Maven(this, config.build.src)
                    def pom = readMavenPom file: 'app/pom.xml'
                    //!set creates a long release name for archiving with job name, version, build number
                    // and commit id, e. g. PetClinic_1.3.1_12_e4655456j
                    releaseName = "${env.JOB_NAME}_${pom.version}_${env.BUILD_NUMBER}_${env.GIT_COMMIT}"
                    //!set Build name with unique identifier with version and build number id, e. g. "1.3.1_12"
                    setBuildName("${pom.version}_${env.BUILD_NUMBER}")
                    mailParams = ['#BUILD_NAME': releaseName, '#LINK': "${env.JOB_DISPLAY_URL}"]
                    //!- build (maven, gradle, etc.)
                    maven.build()
                }
            }
        }
        stage('SCAN') {
            agent {
                node {
                    //if not specified otherwise, everything runs on the Jenkins master
                    label 'master'
                    //create a custom workspace for every build to avoid interference due to concurrent builds
                    customWorkspace "jobs/${env.JOB_NAME}/${env.BUILD_NUMBER}"
                }
            }
            steps {
                parallel(
                    TQS: {
                        script{
                            //!- unit test
                            maven.unitTest()
                            //!- Source Code Check with SonarQube and TQS-Ruleset
                            // Sonar environment variables should be configured globally inside jenkins oder supplied here via config
                            maven.staticCodeAnalysisSonar()
                        }
                    },
                    SECURITY: {
                        script {
                            def checkmarx = new dhl.tools.CheckMarx_MMS(this, config.checkmarx)
                            checkmarx.run()
                        }
                    },
                    OSLC: {
                        //!- Open Source License Compliance Test
                    },
                    NEXUS: {
                        script {
                            //!- Archive the binaries in Nexus
                            maven.buildPackages()
                            packageurl = maven.uploadToNexus(releaseName, config.upload)
                        }
                    }
                )
            }
        }
        stage('UAT') {
            agent {
                node {
                    //if not specified otherwise, everything runs on the Jenkins master
                    label 'master'
                    //create a custom workspace for every build to avoid interference due to concurrent builds
                    customWorkspace "jobs/${env.JOB_NAME}/${env.BUILD_NUMBER}"
                }
            }
            steps {
                parallel(
					//! Functional Test
                    FUNCTIONAL: {
                        script {
                            //deploy environment for functional tests
                                dir('test/systemtest'){
                                    maven.execMaven(config.selenium, 'verify')
                                }
                            //destroy environment for functional tests
                        }
                    },
                    PERFORMANCE: {
                        script {
                            //deploy environment for performance tests
                            def pfcConfiguration = config.pfc
                            pfcConfiguration.testRunID = releaseName
                            performanceTest(pfcConfiguration)
                            //destroy environment for performance tests
                        }
                    },
                    EXPLORATIVE: {
                        //deploy environment for explorative tests
                    }
                )
            }
        }
        stage('APPROVAL') {
            node none
            when {
                //only commits to master should be deployed to production (this conditions needs a multi-branch-pipeline)
                branch 'master'
            }
            steps {
                parallel(
                    AESYS: {
                        //"Abnahmeerklärung System" (approval from product owner as needed by Phase Gate Model)
                        //!Email to product owner as notification for manual approval of system
                        sendMail(config.mail.AESYS, mailParams)
                        //!Prompt for system approval
                        input(message:'Abnahmeerklärung System', ok: 'Approve', submitter: config.submitter.AESYS)
                        //destroy explorative environment
                    },
                    BFR: {
                        //"Betriebliche Freigabe" (approval from transition manager as needed by Phase Gate Model) - can be left out, when pre-approved Change Template is in use
                        //!Email to transition manager as notification for manual approval of go live
                        sendMail(config.mail.BFR, mailParams)
                        //!Prompt for change approval
                        input(message:'Betriebliche Freigabe', ok: 'Approve', submitter: config.submitter.BFR)
                        //send the results of the TQS scan to TQS via LCM-Upload in your workspace
                        publishTQSReport(config.tqs, config.mail.tqs, mailParams)
                    },
                    //if one approval is denied, then build is failed
                    failFast: true
                )
            }
        }
        stage('PROD') {
            agent {
                node {
                    //if not specified otherwise, everything runs on the Jenkins master
                    label 'master'
                    //create a custom workspace for every build to avoid interference due to concurrent builds
                    customWorkspace "jobs/${env.JOB_NAME}/${env.BUILD_NUMBER}"
                }
            }
            when {
                //only commits to master should be deployed to production (this conditions needs a multi-branch-pipeline)
                branch 'master'
            }
            steps {
                // This step aborts builds if they reach this step after newer builds
                milestone(ordinal: 0, label: 'PROD')
                script {
                    //!Change-Creation (IT-S in GSN, TSI in SM9)
                    //lock PROD environment since you can only deploy once at a time
                    lock(resource: "$JOB_NAME-prod_env"){
                        //Deployment (per node)
                        curlDownload(config.deployment, packageurl,'$user')
                        //- Flightcheck (per node)
                        //- Check Monitoringevents and Logfiles (per node)
                    }
                    //!Change-Completion
                }
            }
        }
    }
    post {
        failure {
            //!Notify team and abbort Change if needed
            //TODO: artf453771
            handleError(config.mail.error, mailParams)
        }
        always {
            //!delete workspace
            node('master') {
                dir("jobs/${env.JOB_NAME}/${env.BUILD_NUMBER}") {
                    deleteDir()
                }
            }
        }
    }
}