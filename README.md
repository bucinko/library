# CDlib 0.11
This Shared Library extends the Jenkins Pipeline with useful steps for using Maven, CheckMarx 
and other utilities.

![overview](https://lcm.deutschepost.de/sf/wiki/do/viewAttachment/projects.testfactory_infrastruktur/wiki/CDBibliothek/CD_View.png)

## Requirements 
### Jenkins
In order to use the CDlib you will of course need a Jenkins server. 
The library is built and tested with Jenkins 2.60+. 
We recommend using the LCM-distributed Jenkins.

### Firewall
To reach our git-repository your Jenkins must have firewall clearance for `lcm-scms2.deutschepost.de (10.192.35.138 - Prague-NAT: 10.219.13.105)`.

### User
You will also need a user able to checkout the git-repository. 
This will be provided by `helpdeskbrief@deutschepost.de` with subject: `LCM | project Test Factory | create new technical user and grant access to role cdlib_user`. 
In case of problems, please contact `testfactory@deutschepost.de` or Steve Lohr. 

### Plugins
- Jenkins 2.60+
- Config File Provider Plugin	2.13+
- Credentials Binding Plugin	1.10+
- Folders Plugin	5.13+
- Git Client Plugin	2.1.0+
- Lockable Resources Plugin	1.10+
- Mailer Plugin	1.18+
- Pipeline	2.4+
- Pipeline Utility Steps	1.2.0+
- Pipeline: Basic Steps	2.3+
- Pipeline: Input Step	2.5+
- Pipeline: Job	2.9+
- Pipeline: Milestone Step	1.2+
- Pipeline: Shared Groovy Libraries	2.5+
- Pipeline: Stage View Plugin	2.3+
- SonarQube Plugin	2.5+
- SSH Agent Plugin	1.13+
- Timestamper	1.8.7+
- xUnit Plugin	1.102+
- BlueOcean 1.2+

### Configuration
The CDlib is provided via git and can be configured on Jenkins globally and on folder-level. 
Globally configured libraries are trusted, therefore there is no script-approval for security relevant methods,
 whereas a library on folder level isn't. When you configure a library globally and locally (folder-level) with the same name, 
 the folder-library will be used - this will be logged with `Only using first definition of library WF-LIB`. 
 If you want to specify a specific version for a pipeline you can check `Allow default version to be overridden` and 
 load the library with for example `@Library('WF-LIB@0.10') _` in your script. 
 A version is the name of a tag, branch or the commit ID (revision). 


## Classes [(local)](src/dhl/tools/README.md) [(lcm)](code/git/projects.testfactory_infrastruktur/scm.tf_cd_standardbibliothek/tree/src/dhl/tools)
- CheckMarx
- Maven

## Pipeline Steps [(local)](vars/README.md) [(lcm)](code/git/projects.testfactory_infrastruktur/scm.tf_cd_standardbibliothek/tree/vars)
- checkoutSCM
- createGitTag
- curlDownload
- exec
- execRemote
- find
- findFiles
- getResources 
- handleError 
- loadConfig
- printDebug
- publishTQSReport 
- rm 
- sendMail 
- setBuildName