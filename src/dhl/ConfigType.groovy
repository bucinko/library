package dhl

enum ConfigType {
    ALL(""),
    DEPLOYMENT("deployment"),
    CHECKOUT("checkout"),
    UPLOAD("upload"),
    SONAR("sonar")

    final String configPart

    private ConfigType(String configPart) {
        this.configPart = configPart
    }
}