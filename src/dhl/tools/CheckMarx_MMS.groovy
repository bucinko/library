package dhl.tools

import org.codehaus.groovy.runtime.DateGroovyMethods
import org.codehaus.groovy.runtime.EncodingGroovyMethods
import hudson.AbortException


class CheckMarx_MMS implements Serializable {
    final script

    /* Custom Vars */
    final String globalDir
    /* zip file */
    final String includePattern
    final String excludePattern

    /* scan options */
    final boolean dynamicScan
    final boolean staticScan
    final boolean reporting
    final boolean incremental
    final boolean importToTFS
    final boolean evalQualityGate

    final String projectId
    final String jobManager
    final String bootUpTimeout
    final String delayTime
    final String generatingReportTimeout
    final String maxLowFindings
    final String maxMediumFindings
    final String maxHighFindings
    final String responseTimeout
    final String timeoutLimit
    final String urls

    /* download */
    final String downloadCredentialsID
    final String lookupName
    final String proxyURL
    final String proxyPort
    final boolean proxySet
    final int pollSleepTime
    final int pollRetries

    CheckMarx_MMS(script, Map config = [:]) {
        assert script
        this.script = script
        globalDir = config.dir ?: 'app'
        /* zip file */
        includePattern = config.includePattern ?: "*src*"
        excludePattern = config.excludePattern ?: "*.git* *.svn* *test* *.jpeg* *.jpg* *.gif* *.svg* *.png* *.jar* *.woff* *.otf* *.ttf* *.pdf* *.mpg* *.doc* *.txt*"

        /* scan options */
        /* don't replace with elvis operator because of groovy truth */
        dynamicScan = config.dyanmicScan != null ?  config.dynamicScan : false
        staticScan = config.staticScan != null ? config.staticScan : true
        reporting = config.reporting != null ? config.reporting : true
        incremental = config.incremental != null ?  config.incremental : false
        importToTFS = config.importToTFS != null ?  config.importToTFS : false
        evalQualityGate = config.evalQualityGate != null ?  config.evalQualityGate : true
        /* there a no possible default values for projectId and jobManager there assert that there passed as parameters */
        assert config.projectId
        assert config.jobManager
        projectId = config.projectId
        jobManager = config.jobManager
        bootUpTimeout = config.bootUpTimeout ?: '300'
        delayTime = config.delayTime ?: '300'
        generatingReportTimeout = config.generatingReportTimeout ?: '5'
        maxLowFindings = config.maxLowFindings ?: '15'
        maxMediumFindings = config.maxMediumFindings ?: '0'
        maxHighFindings = config.maxHighFindings ?: '0'
        responseTimeout = config.responseTimeout ?: '300'
        timeoutLimit = config.timeoutLimit ?: '3600'
        urls = config.urls ?: ''

        /* download */
        downloadCredentialsID = config.downloadCredentialsID ?: 'CM-MMS-Downloaduser'
        lookupName =  config.lookupName ?: 'MMS_Project'
        proxyPort = config.proxyPort ?: ''
        proxyURL = config.proxyURL ?: ''
        pollSleepTime = config.pollSleepTime ?: 15
        pollRetries = config.pollRetries ?: 20
        // use proxy if any property is set
        proxySet = proxyURL || proxyPort
    }

    void run() {
        scan()
        download()
    }

/* INTERNAL FUNCTIONS */
    private void scan() {
        /* delete old zip and create a new one */
        String zipPath = "${env.WORKSPACE}/checkmarx.zip"
        rm zipPath
        //TODO replace by jenkins zip
        exec "zip -9 -q -r ${zipPath} ${globalDir} -i ${includePattern} -x ${excludePattern}"

        boolean success = performScan bootUpTimeout: bootUpTimeout,
                doDynamicScan: dynamicScan,
                doStaticScan: staticScan,
                doReportGenerating: reporting,
                doIncrementalStaticScan: incremental,
                doTfsImport: importToTFS,
                delayTime: delayTime,
                filepath: zipPath,
                generatingReportTimeout: generatingReportTimeout,
                jobManager: jobManager,
                maxLowFindings: maxLowFindings,
                maxMediumFindings: maxMediumFindings,
                maxHighFindings: maxLowFindings,
                projectId: projectId,
                responseTimeout: responseTimeout,
                timeoutLimit: timeoutLimit,
                urls: urls

        rm zipPath

        if (evalQualityGate) {
            if(!success) {
                throw new AbortException("[ERROR] Quality Gate failed.")
            }
        }
    }

    private void download(){
        byte[] uspass
        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: downloadCredentialsID, passwordVariable: 'PASS', usernameVariable: 'USR']]) {
            uspass = "${env.USR}:${env.PASS}".getBytes()
        }
        String remoteAuth = "Basic " + EncodingGroovyMethods.encodeBase64(uspass).toString()

        String dateFormat = DateGroovyMethods.format(new Date(), 'yyyyMMdd')

        String remoteUrl = "https://sec-scan.t-systems-mms.eu/Reports/${lookupName}/SecurityReport_${lookupName}_${dateFormat}.pdf"
        String filename = "SecurityReport_${lookupName}_${dateFormat}.pdf"
        startDownload(filename, remoteUrl, remoteAuth)

        remoteUrl = "https://sec-scan.t-systems-mms.eu/Reports/${lookupName}/SecurityReport_${lookupName}_${dateFormat}.xlsm"
        filename = "SecurityReport_${lookupName}_${dateFormat}.xlsm"
        startDownload(filename, remoteUrl, remoteAuth)
    }

    private void startDownload(String filename, String remoteUrl, String remoteAuth) {
        echo """\
                [INFO] Checkmarx
                [INFO] Zieldatei: ${filename}
                [INFO] Quelldatei: ${remoteUrl}
                [INFO] Proxy gesetzt? ${proxySet}""".stripIndent()

        retry(pollRetries) {
            URL url = new URL(remoteUrl)
            Proxy proxy
            if (proxySet) {
                proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyURL, proxyPort.toInteger()))
            } else {
                proxy = Proxy.NO_PROXY
            }
            URLConnection conn = url.openConnection(proxy)
            conn.setRequestProperty("Authorization", remoteAuth)
            int code = conn.getResponseCode()
            echo "[INFO] Rufe URL auf - Statuscode: " + code

            if (code == 200) {
                echo "[INFO] Datei vorhanden, versuche den Download"
                File report = new File("${env.WORKSPACE}/${filename}")
                /* opens input stream from the HTTP connection and writes it into the file */
                    InputStream inputStream = conn.getInputStream()
                report << inputStream.bytes
                /* closing and nulling in necessary for serialization */
                inputStream.close()
                inputStream = null
                conn.disconnect()
                conn = null
                proxy = null
                url = null

                if (report.size() > 0) {
                    echo "[INFO] Download abgeschlossen"
                    echo "[INFO] Archiviere Datei: ${filename}"
                    archive filename
                } else {
                    echo "[WARNING] Datei $filename nicht gefunden"
                    sleep time: pollSleepTime, unit: 'SECONDS'
                    throw new FileNotFoundException('Datei nicht verfügbar')
                }
            } else {
                conn.disconnect()
                conn = null
                proxy = null
                url = null
                echo '[WARNING] Datei nicht verfügbar'
                sleep time: pollSleepTime, unit: 'SECONDS'
                throw new FileNotFoundException('Datei nicht verfügbar')
            }
        }
    }

    /* delegates function calls, which can't be found in the Maven class to the workflow script class */
    def invokeMethod(String name, Object args) {
        script.invokeMethod(name, args)
    }

    /* delegates property look ups, which can't be found in the Maven class to the workflow script class */
    def propertyMissing(String name) {
        script.getProperty(name)
    }
}