package dhl.tools

import dhl.validate.Validate

class Maven implements Serializable {
    final def script
    final String globalTool
    final String globalPropertiesFlat
    final String globalGoals
    final String globalProfilesFlat
    final String globalParameterFlat
    final String globalDir
    final Map xUnitConfig
    final boolean useCoverage
    final Map xUnitConfigDefault = [
            testTimeMargin: '3000', thresholdMode: 1, failureNewThreshold: '0', failureThreshold: '0',
            unstableNewThreshold: '0', unstableThreshold: '0', skipFailureNewThreshold: '0', skipFailureThreshold: '0',
            skipUnstableNewThreshold: '0', skipUnstableThreshold: '0', deleteOutputFiles: false, failIfNotNew: true,
            pattern: '**/target/surefire-reports/*.xml', skipNoTestFiles: true, stopProcessingIfError: false
    ]

    Maven(script, Map configuration = [:]) {
        assert script
        this.script = script
        globalTool = configuration.mavenTool ?: 'Maven 3.x'
        globalGoals = configuration.goals ?: ''
        globalProfilesFlat = configuration.profiles?.join(',') ?: ''
        globalParameterFlat = configuration.parameter?.join(' ') ?: ''
        globalDir = configuration.dir ?: 'app'
        useCoverage = configuration.useCoverage ?: true
        String propertiesFlat = ''
        for (property in configuration.properties) {
            propertiesFlat += " -D$property"
        }
        globalPropertiesFlat = propertiesFlat
        xUnitConfig = xUnitConfigDefault + (configuration.xUnitConfig ?: [:])
    }


    void execMaven(Map localConf = [:], String goals, boolean overwriteGlobalParameter = false) {
        String executable = tool(globalTool) + '/bin/mvn --batch-mode '

        //Profiles
        def profiles_list = [globalProfilesFlat, localConf.profiles ?: []].flatten()
        String profiles = profiles_list?.join(',') ?: ''
        if (profiles) {
            profiles = '--activate-profiles ' + profiles
        }

        //Properties
        String properties = globalPropertiesFlat ?: ''
        for (property in localConf.properties) {
            properties += " -D$property"
        }

        //Parameter
        def parameter_list = [globalParameterFlat, localConf.parameter ?: []].flatten()
        if (overwriteGlobalParameter) {
            parameter_list = localConf.parameter ?: []
        }
        String parameter = parameter_list?.join(' ') ?: ''

        /** don't apply the jacoco goal, when the parameter list gets overwritten by upload to nexus
         *  because the jacoco goals needs a pom.xml
         */
        def jacocoParam = ''
        if (useCoverage && !overwriteGlobalParameter){
            jacocoParam =  'org.jacoco:jacoco-maven-plugin:prepare-agent'
        }
        //Execution
        dir(localConf.dir ?: globalDir) {
            exec "${executable} ${parameter} ${profiles} ${jacocoParam} ${globalGoals} ${goals} ${properties}"
        }
    }

    void build(Map localConf = [:]) {
        localConf.properties = localConf.properties ?: [] << 'skipTests'
        execMaven(localConf, 'clean compile')
    }

    void unitTest(Map localConf = [:], boolean publish = true) {
        if (publish) {
            localConf.properties = localConf.properties ?: [] << 'maven.test.failure.ignore=true'
        }
        execMaven(localConf, 'test')
        if (publish) {
            /* Merge default config, with local config */
            Map conf = xUnitConfig + (localConf.xUnitConfig ?: [:])
            step([$class: 'XUnitBuilder', testTimeMargin: conf.testTimeMargin, thresholdMode: conf.thresholdMode,
                  thresholds: [[$class: 'FailedThreshold', failureNewThreshold: conf.failureNewThreshold,
                                failureThreshold: conf.failureThreshold, unstableNewThreshold: conf.unstableNewThreshold,
                                unstableThreshold: conf.unstableThreshold],
                               [$class: 'SkippedThreshold', failureNewThreshold: conf.skipFailureNewThreshold,
                                failureThreshold: conf.skipFailureThreshold,
                                unstableNewThreshold: conf.skipUnstableNewThreshold,
                                unstableThreshold: conf.skipUnstableThreshold]],
                  tools: [[$class: 'JUnitType', deleteOutputFiles: conf.deleteOutputFiles,
                           failIfNotNew: conf.failIfNotNew, pattern: conf.pattern, skipNoTestFiles: conf.skipNoTestFiles,
                           stopProcessingIfError: conf.stopProcessingIfError]]])
            if (currentBuild.result == 'FAILURE') {
                    error "Unit-Tests failed."

            }
        }
    }

    void buildPackages(Map localConf = [:]) {
        localConf.properties = localConf.properties ?: [] << 'skipTests'
        execMaven(localConf, 'package')
    }

    void staticCodeAnalysisSonar(Map sonarConfig = [:]) {

        String sonarHostUrl = ''
        String sonarLogin = ''
        String sonarPassword = ''
        String userCredentialsId = ''


        if (!sonarConfig.userCredentialsId && !sonarConfig.hostUrl) {
            withSonarQubeEnv {
                sonarHostUrl = env.SONAR_HOST_URL
                sonarLogin = env.SONAR_LOGIN
                sonarPassword = env.SONAR_PASSWORD
            }
        } else {
            userCredentialsId = sonarConfig.userCredentialsId ?: "SONAR-USER"

            sonarHostUrl = sonarConfig.hostUrl ?: 'https://lcm.deutschepost.de/sonar/'
            withCredentials([usernamePassword(credentialsId: userCredentialsId, passwordVariable: 'SONAR_PASSWORD', usernameVariable: 'SONAR_LOGIN')]) {
                sonarLogin = env.SONAR_LOGIN
                sonarPassword = env.SONAR_PASSWORD
            }
        }
        Map localConf = [properties: 
                            ["sonar.host.url=${sonarHostUrl}",
                             "sonar.login=${sonarLogin}",
                             "sonar.password=${sonarPassword}"
                            ],
                            dir: sonarConfig.dir
                        ]
        if (sonarConfig['extraProperties']) {
                localConf['properties'].addAll(sonarConfig['extraProperties'])
        }


        if (!sonarConfig.userCredentialsId && !sonarConfig.hostUrl) {
            withSonarQubeEnv {
                execMaven(localConf, 'sonar:sonar')
            }
        } else {
            // DON'T remove this block. It is essential for masking the passwords in the log output
            withCredentials([usernamePassword(credentialsId: userCredentialsId, passwordVariable: 'SONAR_PASSWORD', usernameVariable: 'SONAR_LOGIN')]) {
                execMaven(localConf, 'sonar:sonar')
            }
        }

        archive '**/target/**/TQS_Reports*.zip'
    }

    Map uploadToNexus(String version, Map uploadConfig) {
        String repoUrl = Validate.notEmpty(uploadConfig.repoUrl, 'A repository url must be provided.')
        String repoId = Validate.notEmpty(uploadConfig.repoId, 'A repository id must be provided.')
        String defaultGroupId = Validate.notEmpty(uploadConfig.defaultGroupId, 'A default group Id must be provided.')
        Map urls = [:]

        for (artifact in uploadConfig.artifacts) {
            String nexusSettings = uploadConfig.nexusSettingsId
            String artifactId = artifact.artifactId
            String packaging = artifact.packaging
            String[] files = new FileNameFinder().getFileNames(pwd(), artifact.filePattern)
            if (files == null || files.length != 1) {
                error "more than one or zero artifacts found: " + artifact.filePattern
            }

            String file = files[0]
            String groupId = artifact.groupId ?: defaultGroupId
            wrap([$class: 'ConfigFileBuildWrapper', managedFiles: [[fileId: nexusSettings, replaceTokens: false, targetLocation: '', variable: 'SETTINGS']]]) {
                Map localConf = [properties: ["file=\"$file\"",
                                              "url=$repoUrl",
                                              "repositoryId=$repoId",
                                              "groupId=$groupId",
                                              "artifactId=$artifactId",
                                              "version=$version",
                                              "packaging=$packaging",
                                              'generatePom=true'],
                                 parameter : ["-q", "-s  \"${env.SETTINGS}\""],
                                dir: uploadConfig.dir ]
                execMaven(localConf, "deploy:deploy-file", true)
            }
            String groupPath = groupId.replaceAll("\\.", "/")
            urls["${groupId}.${artifactId}"] = "${repoUrl}${groupPath}/${artifactId}/${version}/${artifactId}-${version}.${packaging}"
        }
        return urls
    }

    /* delegates function calls, which can't be found in the Maven class to the workflow script class */
    def invokeMethod(String name, Object args) {
        script.invokeMethod(name, args)
    }

    /* delegates property look ups, which can't be found in the Maven class to the workflow script class */
    def propertyMissing(String name) {
        script.getProperty(name)
    }
}
