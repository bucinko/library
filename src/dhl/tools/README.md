# CheckMarxMMS
Checkmarx is a security testing tool for code. This one is hosted and maintained by the T-System MMS. At the moment you need a property-file, explained down this file, to start the check.
The parameters are managed by the config file. 
The property file will be generated during the execution of the workflow with parameters supplied by the config file. 
## Dependencies
- Configured MMS-Checkmarx-Project.

## Configuration
Parameters with a default value are optional and those marked with a * are mandatory. 

| Parameter | Default value | Description |
| -------- | ------------- | ----------- |
| downloadCredentialsID | CM-MMS-Downloaduser | CredentialsId that provides access to username and password for  accessing the checkmarx server to download the security report |
| excludePattern | *.git* *.svn* *test* *.jpeg* *.jpg* *.gif* *.svg* *.png* *.jar* *.woff* *.otf* *.ttf* *.pdf* *.mpg* *.doc* *.txt* | Pattern of files to be excluded from the security check. |
| includePattern | *src* | Pattern of files to be included from the security check. |
| lookupName | MMS_Project | Name of the checkmarx project name. |
| proxyURL | (none) | URL of the proxy server |
| proxyPort | (none) | Port of the proxy server |
| extraParams | (none) | Additional parameter for the invocation of the panther. jar |
| pollSleepTime | 15 | Seconds that will be waited before the next attempt of downloading the test results |
| pollRetries | 20 | Number of attempts to download the test results |
| dynamicScan | false | Enables dynamic scan |
| staticScan | true | Enables static scan |
| reporting | true | Enable generating the report |
| incremental | false | Enables incremental scanning (only useful with occasinally fullscans) |
| importToTFS | false | Enables the importing to Team Foundation Server or Jira |
| evalQualityGate | false | Aborts the build if the quality gate is not reached |
| maxLowFindings | '15' | Maximum allowed low findings. Only used if evalQualityGate is set |
| maxMediumFindings | '10' | Maximum allowed medium findings. Only used if evalQualityGate is set |
| maxHighFindings | '5' | Maximum allowed high findings. Only used if evalQualityGate is set |
| projectId * | (none) | Project key for Checkmarx |
| jobManager * | (none) | Identifier of the report generator |
| bootUpTimeout | '300' | Startup timeout of the Checkmarx engine |
| delayTime | '60' | Delay timeout after the network package will be send again |
| generatingReportTimeout | '5' | Sleep time after the scan |
| responseTimeout | '300' | Network timeout |
| urls | '' | Urls to be used for the Checkmarx scan |
| dir | 'app' | Directory which will be zipped and uploaded to the Checkmarx server |

generatingReportTimeout will just sleep for X seconds after finishing the scan and hopes that the reports are generated after the sleep.
It is better to keep the generatingReportTimeout low and use the pollSleepTime and pollRetries to check periodically if the reports are generated.
This helps to keeps the overall runtime of the security tests as low as possible.

## Example
### Code
```
def checkmarx = new dhl.tools.CheckMarx_MMS(this, configuration.checkmarx)
checkmarx.run()
```
### Configuration
```
checkmarx : [
    lookupName:'MMS_Project',
    proxyURL:'proxy.tcb.deutschepost.de',
    proxyPort:'8080',
    projectId: '50087',
    jobManager: 'test_project_file'
]
```
### Result
```
13:28:26 [checkmarx] [workspace] Running shell script
13:28:27 [checkmarx] + cd /var/lib/jenkins/jenkins1/jobs/CDlib_master/jobs/CD-Bibliothek-Pipeline-Modulstruktur/workspace/_checkmarx
13:28:27 [checkmarx] + '[' -f panther.jar ']'
13:28:27 [checkmarx] + '[' -f demoprojekt.properties ']'
13:28:27 [checkmarx] + echo '[INFO] Starte Checkmarx Scan'
13:28:27 [checkmarx] [INFO] Starte Checkmarx Scan
13:28:27 [checkmarx] + java -jar panther.jar -r -s -c demoprojekt.properties -u **** -p ****
13:28:27 [checkmarx] 31.01.2017 13:28:26 [ INFO] (         LoggerUtils.java:68 ): +--------------------------------------+
13:28:27 [checkmarx] 31.01.2017 13:28:26 [ INFO] (         LoggerUtils.java:69 ):              P4N7H3R v1.2.2
13:28:27 [checkmarx] 31.01.2017 13:28:26 [ INFO] (         LoggerUtils.java:70 ): +--------------------------------------+
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:160):  Time:
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:161):      + Tue Jan 31 13:28:26 CET 2017
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:162): +--------------------------------------+
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:163):  Todo:
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:164):      + dynamic scan:             false
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:165):      + static scan:              true
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:166):      + generating reports:       true
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:167):      + import to TFS             false
13:28:27 [checkmarx] 31.01.2017 13:28:26 [DEBUG] (         Application.java:168): +--------------------------------------+
13:28:27 [checkmarx] 31.01.2017 13:28:26 [ INFO] (         LoggerUtils.java:68 ): +--------------------------------------+
13:28:27 [checkmarx] 31.01.2017 13:28:26 [ INFO] (         LoggerUtils.java:69 ):               STATIC SCAN
13:28:27 [checkmarx] 31.01.2017 13:28:26 [ INFO] (         LoggerUtils.java:70 ): +--------------------------------------+

...

13:38:05 [checkmarx] 31.01.2017 13:37:59 [ INFO] (         LoggerUtils.java:68 ): +--------------------------------------+
13:38:05 [checkmarx] 31.01.2017 13:37:59 [ INFO] (         LoggerUtils.java:69 ):          STATIC SCAN COMPLETED
13:38:05 [checkmarx] 31.01.2017 13:37:59 [ INFO] (         LoggerUtils.java:70 ): +--------------------------------------+
13:38:05 [checkmarx] 31.01.2017 13:37:59 [ INFO] (         LoggerUtils.java:68 ): +--------------------------------------+
13:38:05 [checkmarx] 31.01.2017 13:37:59 [ INFO] (         LoggerUtils.java:69 ):                COMPLETED
13:38:05 [checkmarx] 31.01.2017 13:37:59 [ INFO] (         LoggerUtils.java:70 ): +--------------------------------------+
13:38:05 [checkmarx] + rm -f demoprojekt.zip
[Pipeline] [checkmarx] }
[Pipeline] [checkmarx] // withCredentials
[Pipeline] [checkmarx] withCredentials
[Pipeline] [checkmarx] {
[Pipeline] [checkmarx] }
[Pipeline] [checkmarx] // withCredentials
[Pipeline] [checkmarx] echo
13:38:05 [checkmarx] [INFO] Starte Download Checkmarx-Ergebnis
[Pipeline] [checkmarx] echo
13:38:05 [checkmarx] [INFO] Checkmarx
13:38:05 [checkmarx] [INFO] Zieldatei: /var/lib/jenkins/jenkins1/jobs/CDlib_master/jobs/CD-Bibliothek-Pipeline-Modulstruktur/workspace/_checkmarx/SecurityReport_MMS_Project_20170131.pdf
13:38:05 [checkmarx] [INFO] Quelldatei: [https://sec-scan.t-systems-mms.eu/Reports/MMS_Project/SecurityReport_MMS_Project_20170131.pdf]
13:38:05 [checkmarx] [INFO] Proxy gesetzt? true
13:38:05 [checkmarx]
[Pipeline] [checkmarx] retry
[Pipeline] [checkmarx] {
[Pipeline] [checkmarx] echo
13:38:05 [checkmarx] [INFO] Rufe URL auf - Statuscode: 200}}}
```

# Maven
Maven is a build tool for java applications. This pipeline step encapsulates the invocation of the Maven build tool. It features the possibilities to build, package, test, SonarQube scan and uploading the resulting application to a Nexus server. 

## Configuration
| Parameter | Default Value | Example | Description |
| --------- | ------------- | ------- | -----------  |
|mavenTool|Maven 3.x| Maven | Name of the maven installation in Jenkins |
|goals|(none)| 'clean compile'|  Additional goals that will be always executed |
|profiles|(none)| ['dev','onlyBlueTests'] | List of maven profiles  |
|parameter|(none)| ['-q','-T4']  | List of maven parameters |
|properties|(none)|['dbName=testDB','dbHost=somehost']  | List of maven properties. Each property will be prefixed with ''-D'' automatically |
|dir|'app'| 'myFancyAppDirectory' | Directory where all commands will be executed. Can be overwritten for each command in the localConf |
|useCoverage|true|true|Enables code coverage in unit tests |
|xUnitConfig|(Displayed in the table below)| | List of settings and thresholds for the xunit Jenkins plugin |

### xUnitConfig default values 
There are two kinds of thresholds for failed tests and for skipped tests.
If one thresholds is exceeded the build will fail or marked as unstable. 
The threshold can be set to a maximum for example no more than 100 failed tests in total (failureThreshold: '100'). 
It is possible to set a delta threshold for example no more than 10 newly failed tests (failureNewThreshold: '10'). 

| Parameter | Default Value |
| --------- | ------------- |
| testTimeMargin | '3000' |
| thresholdMode | 1 |
| failureNewThreshold | '0' |
| failureThreshold | '0' |
| unstableNewThreshold | '0' |
| unstableThreshold | '0' |
| skipFailureNewThreshold | '0' |
| skipFailureThreshold | '0' |
| skipUnstableNewThreshold | '0' |
| skipUnstableThreshold | '0' |
| deleteOutputFiles | false |
| failIfNotNew| true |
| pattern | '\*\*/target/surefire-reports/\*.xml' |
| skipNoTestFiles| true |
| stopProcessingIfError| false |

## Methods
In the localConf properties, parameter and profiles can be additionally specified.
They will be used additionally to  global options, which  are set during the creation of the Maven object.
  
| Name | Parameter | Description |
| -----| --------- | ------------ |
|Maven|WorkflowScript script, Map configuration = [:]| Constructs the Maven object with the WorkflowScript and Map of configuration parameters. Uses default values if not set. |
|execMaven|Map localConf = [:], String goals, overwriteGlobalParameter = false| Executes the Maven tool with the given goals and uses global and local parameters. It is possible to overwrite global parameters for this execution. |
|build| Map localConf = [:] | Invokes execMaven with the goals `clean compile`. it is possible to supply some localConfiguration |
|buildPackages|Map localConf = [:]| Invokes execMaven with the goal `package`. it is possible to supply some localConfiguration |
|staticCodeAnalysisSonar|Map sonarConfig = [:] | Executes the SonarQube scan |
|uploadToNexus|String version, Map uploadConfig | Uploads the artifacts in the config with the given version to the Nexus server. Returns key value pairs of the artifacts with the URL. |

### sonarConfig
The parameters dbCredentialsId and jdbcUrl are obsolete.

| Parameter | Default Value | Description |
| --------- | ------------- | ----------  |
|userCredentialsId|SONAR-USER| Jenkins CredentialsID for the SonarQube |
|hostUrl|https://lcm.deutschepost.de/sonar/ | URL of the SonarQube instance |
|extraProperties|(none)| It is possible to define additional properties that will be added to the maven invocation (e.g. 'sonar.sourceEncoding=UTF8' ) |
|dir| Directory where this command will be executed |

### uploadConfig (for the Nexus server)
| Parameter | Description |
| --------- | ----------  |
|repoUrl|  URL of the Nexus server |
|repoId| ID of the repository |
|defaultGroupId|default group id |
|nexusSettingsId| settings id |
|artifacts|  List of artifacts (described below) |
|dir| Directory where this command will be executed |

#### Artifact 
| Parameter | Description |
| --------- | ----------  |
|artifactId| id of the artifact |
|packaging| packaging of the artifact |
|filePatter| Ant style file pattern |
|groupId| group id of the artifact |
  
  
## Example
```
/* creates a new checkmarx object with a reference of the WorkflowScript(this) and the configuration */
def maven = new dhl.tools.Maven(this, parameter:['-q'],profiles:['profile1','profile2']])
/* builds to application */
maven.build()
/* runs unit tests */
maven.unitTest()
/* checks sonar */
maven.staticCodeAnalysisSonar()
/* creates the package */
maven.buildPackages()
/* uploads the packages to the Nexus server */
maven.uploadToNexus("1.3.3.7",  configuration.upload)
```

