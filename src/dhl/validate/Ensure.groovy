package dhl.validate;

@com.cloudbees.groovy.cps.NonCPS
static <T> T notNull(T param, T defaultParam){
	if (param == null){
		return defaultParam
	}else {
		return param
	}
}

@com.cloudbees.groovy.cps.NonCPS
static <T> T notEmpty(T param, T defaultParam){
	if (!param){
		return defaultParam
	} else{
		return param
	}
}

