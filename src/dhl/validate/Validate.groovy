package dhl.validate;

@com.cloudbees.groovy.cps.NonCPS
static<T> T notNull(T param, message){
	if (param == null){
		throw new IllegalArgumentException(message)
	}
	return param
}

@com.cloudbees.groovy.cps.NonCPS
static <T> T notEmpty(T param, message){
	if (!param){
		throw new IllegalArgumentException(message)
	}
	return param
}

