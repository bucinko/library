package dhl.tools

import spock.lang.Specification

class CheckMarx_MMSTest extends Specification {

    final def script = new Expando(
            env: [WORKSPACE : 'workspace@test'],
            result : [],
            exec : { cmd -> result << cmd },
            rm : { _ -> }
    )

    def "default values"() {
        when:
        def underTest = new CheckMarx_MMS(script)

        then:
        underTest.configFile == [:]
        underTest.checkmarxSystemID == 'Checkmarx_MMS_User'
        underTest.downloadCredentialsID == 'CM-MMS-Downloaduser'
        underTest.excludePattern == "*.git* *.svn* *test* *.jpeg* *.jpg* *.gif* *.svg* *.png* *.jar* *.woff* *.otf* *.ttf* *.pdf* *.mpg* *.doc* *.txt*"
        underTest.includePattern == "*src*"
        underTest.lookupName == 'MMS_Project'
        underTest.proxyPort == ''
        underTest.proxyURL == ''
        underTest.extraParams == ''
        underTest.pollSleepTime == 15
        underTest.pollRetries == 20
        underTest.pantherVersion == '1.2.2'
        underTest.dynamicScan == false
        underTest.staticScan == true
        underTest.reporting == true
        underTest.incremental == false
        underTest.terminate == true
    }

    def "set booleans to false"() {
        when:
        def underTest = new CheckMarx_MMS(script, [dynamicScan:false, staticScan:false, reporting:false, terminate:false])

        then:
        underTest.dynamicScan == false
        underTest.staticScan == false
        underTest.reporting == false
        underTest.incremental == false
        underTest.terminate == false
    }
}
