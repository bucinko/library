package dhl.tools

import spock.lang.Specification

class MavenTest extends Specification {

    def script = new Expando(
            result : "",
            getResult : { -> result.replaceAll("\\s+", " ") },
            tool : { _ -> "" },
            dir : { _, func -> func() },
            exec : { cmd -> result = cmd }
        )


    def "profileExpansion with one profile and two properties"() {
        when:
            def underTest = new Maven(script, [profiles:['profile1'],properties:['skipTests','http.proxyPort=8080'],
                                       parameter:['-f pom.xml']])
            
        then:
            underTest.globalProfilesFlat == "profile1"
            underTest.globalPropertiesFlat == " -DskipTests -Dhttp.proxyPort=8080"
            underTest.globalParameterFlat == "-f pom.xml"
    }
    
    def "profileExpansion with multiple profiles, one property and multiple profiles"() {
        when:
            def underTest = new Maven(script, [profiles:['profile1','profile2'],properties:['skipTests'],
                                       parameter:['-f pom.xml','-s /vol/maven/settings.xml']])
            
        then:
            underTest.globalProfilesFlat == "profile1,profile2"
            underTest.globalPropertiesFlat == " -DskipTests"
            underTest.globalParameterFlat == "-f pom.xml -s /vol/maven/settings.xml"
    }
    
    def "profileExpansion without profile, property and commands"() {
        when:
            def underTest = new Maven(script)
        then:
            underTest.globalProfilesFlat == ''
            underTest.globalPropertiesFlat == ''
            underTest.globalParameterFlat == ''
    }

    def "buildPackage tests without arguments"() {
        setup:
            def underTest = new Maven(script)

        when:
            underTest.buildPackages()

        then:
            script.getResult() == "/bin/mvn --batch-mode org.jacoco:jacoco-maven-plugin:prepare-agent package -DskipTests"
    }

    def "buildPackage tests with global and local arguments"() {
        setup:
            def underTest = new Maven(script, [profiles:['profile1','profile2'],parameter:['-f pom.xml'],
                                   properties:["ProxyTrue"]])

        when:
            underTest.buildPackages([profiles:['profile3','profile4'],parameter:['-s /vol/maven/settings.xml']])

        then:
            script.getResult() == "/bin/mvn --batch-mode -f pom.xml -s /vol/maven/settings.xml --activate-profiles profile1,profile2,profile3,profile4 org.jacoco:jacoco-maven-plugin:prepare-agent package -DProxyTrue -DskipTests"
    }

    def "build with global and local arguments"() {
        setup:
            def underTest = new Maven(script, [profiles:['profile1','profile2'],parameter:['-f pom.xml'],
                                           properties:["ProxyTrue"]])

        when:
            underTest.build([profiles:['profile3','profile4'],parameter:['-s /vol/maven/settings.xml']] )

        then:
            script.getResult() == "/bin/mvn --batch-mode -f pom.xml -s /vol/maven/settings.xml --activate-profiles profile1,profile2,profile3,profile4 org.jacoco:jacoco-maven-plugin:prepare-agent clean compile -DProxyTrue -DskipTests"
    }
}