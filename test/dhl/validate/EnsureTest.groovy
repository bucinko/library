package dhl.validate

import spock.lang.Specification
import spock.lang.Unroll;

class EnsureTest extends Specification {

    def "notNull with valid param"(){
        when:
        def result = Ensure.notNull 'TestParam', 'DefaultParam'

        then:
            assert result == 'TestParam'
    }

    def "notNull with invalid param"(){
        when:
        def result = Ensure.notNull null, 'DefaultParam'

        then:
            result == 'DefaultParam'
    }

    @Unroll
    def "notEmpty with #param param"(){
        when:
        def result = Ensure.notEmpty param, 'DefaultParam'

        then:
        assert result == expectedResult

        where:
        param       | expectedResult
        ''          | 'DefaultParam'
        null        | 'DefaultParam'
        'TestParam' | 'TestParam'
    }

}
