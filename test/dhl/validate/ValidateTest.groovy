package dhl.validate

import spock.lang.Specification;
import spock.lang.Unroll;

class ValidateTest extends Specification {

    def "notNull with valid param"() {
        when:
            def result = Validate.notNull 'TestParam', 'Parameter ist nicht optional'

        then:
            result == 'TestParam'

    }

    def "notNull with invalid param"() {
        when:
			Validate.notNull null, expectedMessage

        then:
			IllegalArgumentException ex = thrown()
			ex.message == expectedMessage
		where:
			expectedMessage | _
			'Parameter ist nicht optional' | _
    }

    @Unroll
	def "notEmpty with #param Parameter"() {
        when:
			Validate.notEmpty paramValue, message

        then:
			IllegalArgumentException ex = thrown()
			ex.message == message
		where:
			param  | paramValue | message
			'Null' | null       | 'Parameter ist nicht optional und darf nicht leer sein'
			'Empty'| ''         | 'Parameter ist nicht optional und darf nicht leer sein'
    }

    def "notEmptyWithValidParam"() {
        when:
			def result = Validate.notEmpty 'Nicht leer', 'Parameter ist nicht optional und darf nicht leer sein'

        then:
			result == 'Nicht leer'
    }

}
