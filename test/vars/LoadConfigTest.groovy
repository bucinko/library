import spock.lang.Specification

class LoadConfigTest extends Specification {
  def config
  def loader
  def expectedDeploymentConfig = [host:"host1"]
  def expectedCheckoutConfig = [path:"abc"]
  def expectedConfig = [checkout:expectedCheckoutConfig, deployment:expectedDeploymentConfig]
  
  def setup() {
    config = new loadConfig();
    config.metaClass.pwd = {"somedir"}
    config.metaClass.load = {expectedConfig}

  }

    def "loadConfig without Type should return whole config"(){
        when:
            def result = config.call("Testkonfig")
        then:

            result == expectedConfig
    }
    
    def "loadConfig with ConfigType.Deployment should return deployment config"(){
        
        when:
            def result = config.call("Testkonfig", dhl.ConfigType.DEPLOYMENT)

        then:
            result == expectedDeploymentConfig
    }
    
    def "loadConfig with ConfigType.Checkout should return checkout config"(){
        
        when:
            def result = config.call("Testkonfig", dhl.ConfigType.CHECKOUT)

        then:
            result == expectedCheckoutConfig
    }
    
}

interface fileLoader {
    def fromSVN(String name,String credentialsId ,String svnPath)
}