# Steps
List of pipeline steps.

## checkoutSCM
This DSL-command enables you to specify your checkout with one simple command regardless of the SCMS (git or SVN) and other parameters. It can be combined with our loadConfig-command to make it completely flexible. The checkout-command returns the checked out revision (SVN) or commit-ID (git). 
### Parameter 
| Parameter | Description |
| --------- | ----------- |
| config | multiple configs can be provided, explanation of structure below |
### Config
| Parameter | Optional | Description |
| --------- | -------- | ----------- |
| url | no |  SCMS repository, default is SVN, if URL ends with .git, a git-checkout will be performed |
| poll | no | poll the repository for changes and start the pipeline |
| dir | yes | path relative to Jenkins WorkingDir to which the checkout should happen |
| branch | yes | branch to be checked out, for SVN used format "tags/0.9.5" or "branches/featureartf123456", for GIT you can specify tags, branches or commit IDs |
| credentialsId | yes | credentials ID used to checkout the SCM |
| main | yes |  if there is no main-repository, the revision will be an empty String |
### Example Code
#### Pipeline
```
def configuration = loadConfig("RELEASE_DEMO")
def revision
stage('BUILD'){
    //!- Checkout from SCMS (git, SVN)
    revision = checkoutSCM(configuration["checkout"])
```
#### Config
```
checkout:[
        url:"https://lcm-scms2.deutschepost.de/gerrit/p/tf_cd_demoprojekt.git",
        poll: true,
        credentialsId: "cd_bot"
],
```
#### Result 
```
[Pipeline] stage
[Pipeline] { (BUILD)
[Pipeline] checkout
 > git rev-parse --is-inside-work-tree # timeout=10
Fetching changes from the remote Git repository
 > git config remote.origin.url https://lcm-scms2.deutschepost.de/gerrit/p/tf_cd_demoprojekt.git # timeout=10
Fetching upstream changes from https://lcm-scms2.deutschepost.de/gerrit/p/tf_cd_demoprojekt.git
 > git --version # timeout=10
using GIT_ASKPASS to set credentials TestFactory_CD_Bot (tech)
Setting http proxy: proxy.tcb.deutschepost.de:8080
 > git fetch --tags --progress https://lcm-scms2.deutschepost.de/gerrit/p/tf_cd_demoprojekt.git +refs/heads/*:refs/remotes/origin/*
 > git rev-parse refs/remotes/origin/master^{commit} # timeout=10
 > git rev-parse refs/remotes/origin/origin/master^{commit} # timeout=10
Checking out Revision b2bc3e965874b2e40802c7a66ee6b7a9af1bde6d (refs/remotes/origin/master)
 > git config core.sparsecheckout # timeout=10
 > git checkout -f b2bc3e965874b2e40802c7a66ee6b7a9af1bde6d
 > git rev-list b2bc3e965874b2e40802c7a66ee6b7a9af1bde6d # timeout=10
```

## createGitTag
Creates a tag in the current directory or in a given subdirectory.
###Dependencies
- org.eclipse.jgit
### Parameters 
| Parameter | Description |
| --------- | ----------- |
| gitCredentials | CredentialsID for git (should have writing rights) |
| url | The url without the protocol |
| protocol | The protocol (https, ssh) |
| tag_name | The name of the tag |
| removeTag | Remove a existing tag or not (boolean) |
| path | relative path to subdirectory |
### Example Code
#### Pipeline
```
    createGitTag(configuration['push_latest'])
```
#### Config
```
    push_latest:[
		protocol:'https',
		url:"url.de/to/project.git",
		path:'app',
		gitCredentials:"git-cred-bot",
		tag_name:'0.10.Latest',
		removeTag:true
	] 
```
#### Result
```
    [INFO] Pushing tag '0.10.Latest' to https://url.de/to/project.git
```

## curlDownload
When your binaries are stored in the Nexus, you can deploy them from there and not locally from Jenkins, to ensure that the correct package is archived. This can be combinend with the deployment to a tomcat server with our DSL-command curlDownload. 
### Parameters
| Parameter | Description |
| --------- | ----------- |
| config | configuration, see explanation below |
| urls | Key-Value Set of URLs (like given from uploadToNexus) |
| credentials | credentials ID used to checkout the SCM |
### Config
| Parameter | Optional | Description |
| --------- | -------- | ----------- |
|artifactId|no| complete *ArtifactId* (GA in maven coordinates) has to match key in URLs |
|sshCredentials|yes|credentials to authenticate at remote host |
|host|yes|list of remote host(s) - if empty, host of node will be used |
|user|yes| user for download from Nexus |
|destination|no| target folder on host |
### Example Code
#### Pipeline
```
curlDownload(configuration["deployment"],urls,'nexus-testfactory-user')
```
#### Config
```
[
    artifactId:"de.deutschepost.testfactory.demoprojekt",
    sshCredentials: "devadminCredentials",
    hosts: ["A9007"],
    user: "devadmin"
    destination:"/vol1/tomcat/webapps"
]
```
#### Result 
```
[Pipeline] withCredentials
[Pipeline] {
[Pipeline] isUnix
[Pipeline] sh
[workspace] Running shell script
+ curl -u ****:**** -sS https://lcm.deutschepost.de/nexus/content/repositories/testfactory-release/de/deutschepost/testfactory/demoprojekt/1.1-SNAPSHOT-259/demoprojekt-1.1-SNAPSHOT-259.war 
-o /usr/share/tomcat/webapps/Demoportal.war
```

## exec
To execute command line programs without the need to know which OS you are running on u can use the exec DSL step. It runs the given command as is under *ix via sh or via bat under Windows OS. 
### Parameters
| Parameter | Description |
| --------- | ----------- |
| command | The command to be executed |
### Example Code
#### Pipeline
```
node {
  exec "ls"
}
```

## execRemote
### Parameters
| Parameter | Description |
| --------- | ----------- |
| commands| List of commands to be executed on the remote host |
| host | The url or ip of the remote host |
| user | The user used for the login on the remote host |
| credentials | credentials ID used to access the ssh key | 
### Example Code
#### Pipeline
```
node {
  execRemote(config.cmds, "remoteHost", "devadmin")
}
```
#### Config
```
cmds: [ 
    "/etc/init.d/test_service stop",
    "rm -rf /vol1/test"
]
```

## find
This step returns a list with file names matching the include pattern. 
Additionally an exclude pattern can be used to filter out some files.
For both patterns an `ant style` can be used.
A root path for the search can be specified otherwise the current directory will be used.
### Parameters
| Parameter | Default Value | Description |
| --------- | ------------- | ----------- |
| path | current directory | Root directory for the search request |
| include | | Include Pattern for the search request |
| exclude | '' | Exclude pattern for the search request |
### Example Code
#### Pipeline 
```
node('master') {
    files = find  "/var/lib/jenkins/jenkins1/jobs", "**/CDlib*/**/lastStable/build.xml", "**/*master/**"
    for (f in files) {
        echo f
    }
} 
```
#### Result 
```
[Pipeline] node
Running on master in /var/lib/jenkins/jenkins1/jobs/CDlib_master/jobs/mcwr_test/workspace@3
[Pipeline] {
[Pipeline] echo
/var/lib/jenkins/jenkins1/jobs/CDlib_0.10/jobs/CD-Bibliothek-Petclinic-Integration-Build/lastStable/build.xml
[Pipeline] echo
/var/lib/jenkins/jenkins1/jobs/CDlib_0.10/jobs/CD-Bibliothek-Pipeline-Build/lastStable/build.xml
[Pipeline] echo
/var/lib/jenkins/jenkins1/jobs/CDlib_0.9/jobs/CD-Bibliothek-Petclinic-Integration-Build/lastStable/build.xml
[Pipeline] echo
/var/lib/jenkins/jenkins1/jobs/CDlib_0.9/jobs/CD-Bibliothek-Pipeline-Build/lastStable/build.xml
[Pipeline] }
[Pipeline] // node
```

## findFiles
To search the workspace, or the current working dir for files this function provides the possibility to search for files via their extension. The search always runs on the master node, so it is not usable for files on the remote node. 
### Parameters
| Parameter | Description |
| --------- | ----------- |
| filenameExt | The file extension for the search request |
### Example Code
#### Pipeline
```
node("master") {
     exec "touch test.war"
     def files = findFiles("war")
     echo files
}
```
#### Result 
```
[Pipeline] node
Running on master in /var/lib/jenkins/jenkins1/jobs/CDlib_master/jobs/samplejob/workspace
[Pipeline] {
[Pipeline] isUnix
[Pipeline] sh
[workspace] Running shell script
+ touch test.war
[Pipeline] pwd
[Pipeline] echo
[/var/lib/jenkins/jenkins1/jobs/CDlib_master/jobs/samplejob/workspace/test.war]
[Pipeline] }
[Pipeline] // node
```

## getResources
This function is internal aids to find resources located in this shared libraries resource directory.
### Parameters
| Parameter | Default Value | Description |
| --------- | ------------- | ----------- |
| include | | Include Pattern for the search request |
| exclude | '' | Exclude pattern for the search request |


## handleError
A simple way to handle errors. The function differ between three exceptions:
- RejectedAccessException throws the error for the script approval.
- FlowInterruptedException aborts the builds without sending an email.
- All others send an email. 
## Parameters
| Parameter | Description |
| --------- | ----------- |
| conf | The configuration - see below |
| replace | This map contains key value pairs of variables that should be substituted in mail template |

The replace map is optional and default values vor `#BUILD_NAME` and `#LINK` will be set automatically.
## Configuration
All with variables can be replaced in the body and subject. You only have to insert `#VARIABLE` in the body or subject.
The hash map supplied as vars will substitute each var with the corresponding value.

| Parameter | Description | Mandatory |
| --------- | ----------- | --------- |
| from | The sender of the email | Yes |
| to | The recipient of the email | Yes |
| replyTo | If not set the same as sender | No |
| subject | The subject of the email | No |
| body | The message text | Yes |
### Example Code
#### Pipeline
```
post {
    failure {
        handleError(configuration.mail.error, ['#MY_VAR': "This special file"])
    }
}
```
#### Config
```
mail: [
  error: [
        from : "xyz@xyz.com", to:'the@other.com', replyTo : 'noreply@xyz.xyz',
        subject : '#BUILD_NAME failed. Help him! Now!', 
        body: "Something misbehaved. You should have a look here: #MY_VAR! \n Build URL: #LINK", 
    ]
]
```
#### Result
```
subject:  
    MyBuild failed. Help him! Now!
body:   
    Something misbehaved. You should have a look here: This special file! 
    Build URL: https://lcm.deutschepost.de/jenkins1000010192132011/job/MyBuild/16/ 
    FileNotFoundException 
```



## loadConfig
A pipeline-script contains a lot of configuration regarding users, URLs and other parameters. In order to have a cleaner script and to reuse it for different use cases, for example to have 1 pipeline-script for 2 different pipelines (one for development, one for production), it is handy to swap the configuration into an own config-file. Our DSL-command "loadConfig" enables you to load a config-file inside the pipeline-script. 
### Parameters
| Parameter | Optional | Description |
| --------- | -------- | ----------- |
| name | no | name of config file (with relative path to pipeline script) - file extension .groovy can be dismissed |
| type | yes | default (when not specified) is dhl.ConfigType.ALL, but you can also specify a certain subset like dhl.ConfigType.DEPLOYMENT, dhl.ConfigType.CHEKOUT or dhl.ConfigType.SONAR |
### Example Code
#### Pipeline
```
//load pipeline configuration from same path as pipeline.groovy
def configuration = loadConfig("template_conf_0.9")
```
#### Result 
```
[Pipeline] load
[Pipeline] { (/var/lib/jenkins/jenkins3/jobs/Petclinic (Demoprojekt)/workspace@script/RELEASE_DEMO.groovy)
[Pipeline] }
```

## printDebug
For development purposes it is sometimes feasible to print some debugging infos to the console output. To make this output easier to identify this function adds an prefix to the message. 
### Parameters
| Parameter | Description |
| --------- | ----------- |
| message |  The message which will be printed |
### Example Code
#### Pipeline
```
node {
    printDebug("Some debug info")
}
```
#### Result 
```
[Pipeline] {
[Pipeline] echo
[DEBUG] Some debug info
[Pipeline] }
[Pipeline] // node
```

## publishTQSReport
This DSL command publishes the TQS report into the LCM system. 
If no Report is within the target/sonar directory the build will continue, 
but a warning/error message is printed to the console. 
If the user is not allowed to upload the file, the build will continue, 
but a warning/error message is printed to the console If the document already exists within the LCM directory, 
a new version will be created. 

## Parameters 
| Parameter | Description |
| --------- | ----------- |
| configuration | This map holds the configuration for the TQS upload |
| mailConfig | This map provides the configuration for sending an email in the case of an error e.g. subject, body etc. |
| replace | This map is by default empty and can hold variables to be substituted in the mail |

> For details about mailConfig and replace see the sendMail step.

### configuration map
| Parameter | Description |
| --------- | ----------- |
| credentialsId |  Jenkins-Credential-Id for an account which is allowed to write to the folder |
| folderId | LCM-Folder id for the folder or the absolute path in which the file should be stored |
| project  |  LCM-Project Name. if the name of the project is supplied the Report is uploaded to the standard folder (/System/Release) |
| dir | root directory of the application (default is 'app') |

> You can only specify folderId OR project. If you give both parameters the build will continue, but a warning/error message is printed to the console. 

### ExampleCode
#### Pipeline
```
def config = [
                credentialsId: 'some_lcm_user_id',
                folderId: 'docf772788' // or absolute folderId: '/System/Release'
             ]
def alternativeConfig = [
                credentialsId: 'some_lcm_user_id',
                project: 'Test Factory'
                folderId: '/System/Release',
                dir: 'myapp'
             ]
def mailConfig = [
                    from : "xyz@xyz.com", to:'the@other.com', 
                    replyTo : 'noreply@xyz.xyz',
                    subject : 'TQS Report Faild', 
                    body: "Something misbehaved.", 
                ]
        
publishTQSReport(config, mailConfig)
```
#### Result
On success:
```
10:25:56 + java -jar /var/lib/jenkins/jenkins1/jobs/CDlib_master/jobs/CD-Bibliothek-Pipeline-Build/workspace@libs/TF_CD_LIB/resources/teamforgeupload-0.3.2.jar -H https://lcm.deutschepost.de -U **** -P '****' -M CI/CD-Build docf772788 TQS_Reports_tf_cd_standardbibliothek_v0.11.zip
10:25:58 INFO  de.elego.teamforge.TeamforgeUpload - DONE
```
on error:
```
[WARNING] Project and folderId in configuration please use only one.
```

## rm
This pipeline step deletes a file or directory and returns a boolean if the deletion was successful.
The deletion of a directory is always recursive.
### Parameters
| Parameter | Description |
| --------- | ----------- |
| name | The name of the file/directory |

## sendMail
This pipeline step is wrapper to the mail function and enables the replacement of variables in the template configuration.
### Parameters
| Parameter | Description |
| --------- | ----------- |
| conf | The configuration - see below |
| replace | This map contains key value pairs of variables that should be substituted in mail template |
### Configuration
All with variables can be replaced in the body and subject. You only have to insert `#VARIABLE` in the body or subject.
The hash map supplied as vars will substitute each var with the corresponding value.

| Parameter | Description | 
| --------- | ----------- |
| from | The sender of the email | 
| to | The recipient of the email |
| replyTo | If not set the same as sender |
| subject | The subject of the email |
| body | The message text |

### Example Code
#### Pipeline
```
def source = [
                from: 'testfactory@deutschepost.de',
                replyTo: 'noreply@deutschepost.de',
                to:'example@example.com',
                subject: 'Sourcecode bereitgestellt - #BUILD_NAME',
                body: 'Sourcecode bereitgestellt, Build gestartet. #BUILD_URL '
            ]
def replace = [
                '#BUILD_NAME':  "master-16-e345"
                '#BUILD_URL':  "https://lcm.deutschepost.de/jenkins1000010192132011/job/MyBuild/16/"
              ]
sendMail source, replace
```
#### Result
```
subject:
    Sourcecode bereitgestellt - master-16-e345
body:
    Sourcecode bereitgestellt, Build gestartet.  https://lcm.deutschepost.de/jenkins1000010192132011/job/MyBuild/16/
```

## setBuildName
This pipeline step enables the user to change the name of build.
### Parameters
| Parameter | Description |
| --------- | ----------- |
| displayName |  the new name for the build |
### Example Code
#### Pipeline
```
//!set Build name with unique identifier with Branch, Buildnumber and Revision (Short), e. g. master-12-d31391
build_name = "master-${env.BUILD_NUMBER}-$revision"
setBuildName(build_name)
```
#### Result
![result](https://lcm.deutschepost.de/sf/wiki/do/viewAttachment/projects.testfactory_infrastruktur/wiki/SetBuildName/buildname.png)
