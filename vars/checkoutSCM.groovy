String call (Map config) {
    if (config.url.endsWith(".git")){
       checkoutGit(config)
    } else {
        checkoutSVN(config)
    }
}

@Deprecated
String call (Map config, boolean module) {
    call(config)
}

String checkoutGit(Map config) {
    String branch = config.branch ?: '*/master'
    List extensions = []

    if (config.dir){
        extensions << [$class: 'RelativeTargetDirectory', relativeTargetDir: config.dir]
    }

    if (config.sparsePaths){
        extensions << [$class: 'SparseCheckoutPaths', sparseCheckoutPaths: config.sparsePaths]
    }

    Map result = checkout poll: config.poll, scm: [$class: 'GitSCM', branches: [[name: branch]], doGenerateSubmoduleConfigurations: false, extensions: extensions, submoduleCfg: [], userRemoteConfigs: [[credentialsId: config.credentialsId, url: config.url]]]
    int length = config.revlength ?: 7
    result['GIT_COMMIT'].take(length)
}

String checkoutSVN(Map config) {
    String localDir = config.dir ?: '.'

    if (config.url.endsWith("/")) {
        config.url = config.url.take(config.url.size()-1)
    }

    String remoteUrl = config.url
    if (config.branch){
        remoteUrl += "/${config.branch}"
    } else {
        remoteUrl += "/trunk"
    }

    Map result = checkout  poll: config.poll, scm:  [$class: 'SubversionSCM', locations: [[credentialsId: config.credentialsId, depthOption: 'infinity', local: localDir, remote: remoteUrl]], workspaceUpdater: [$class: 'UpdateUpdater']]
    result['SVN_REVISION']
}
