Checks the repository out and returns the Revision(SVN)/CommitId(GIT)

<h4>Parameter</h4>
<ul>
    <li>
        <b>config</b>
        - Configuration
    </li>
</ul>

<h4>Configuration format</h4>
<ul>
    <li>
        <b>url</b>
        - URL of the remote repository
    </li>
    <li>
        <b>poll</b>
        - if the repository should be polled for changes (boolean)
    </li>
    <li>
        <b>dir</b>
        - relative directory of the current working directory for the checkout
    </li>
    <li>
        <b>branch</b>
        - branch to be checked out; default:master(git), trunk(svn)
    </li>
    <li>
        <b>credentialsId</b>
        - Credentials id to be used for the checkout
    </li>
    <li>
        <b>sparsePaths</b>
        - Specify the paths that you'd like to sparse checkout (git only)
    </li>
</ul>
