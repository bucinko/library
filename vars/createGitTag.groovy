import org.eclipse.jgit.api.Git
import org.eclipse.jgit.api.PushCommand
import org.eclipse.jgit.transport.CredentialsProvider
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider
import org.eclipse.jgit.lib.Ref
import dhl.validate.Validate
import dhl.validate.Ensure

void call(conf){
    String creds = Validate.notNull(conf['gitCredentials'],'You have to set the git credentials.')
    String url = Validate.notNull(conf['url'],'You have to set the url to git - with .git!')
    String prot = Validate.notNull(conf['protocol'],'You have to set the protocol (ssh, https, http).')
    String tag_name = Validate.notNull(conf.tag_name,'You have to set a tag name.')
    Boolean remove = Ensure.notNull(conf['removeTag'], false)

    String path = pwd() + "/" + conf['path'] ?: pwd()
    if (!path.endsWith("/")){
        path = "${path}/"
    }
    path = "${path}.git"

    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: creds, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
        String remote = "${prot}://${env.USERNAME}@${url}"
        UsernamePasswordCredentialsProvider cp = new UsernamePasswordCredentialsProvider("${env.USERNAME}", "${env.PASSWORD}")

        pushTag(cp, remote, path, tag_name, remove)
    }
}

void pushTag(cp, remote, path, tag_name, update){
    try {
        Git git = Git.open( new File ( path ) )   
        git.tag().setName(tag_name).setObjectId(null).setForceUpdate(update).call()

        echo "[INFO] Pushing tag '$tag_name' to $remote"

        PushCommand pc = git.push()
        pc.setCredentialsProvider(cp)
        pc.setRemote(remote)
        pc.setForce(update)
        pc.setPushTags()
        pc.call()

    } catch(org.eclipse.jgit.api.errors.RefAlreadyExistsException e){
        echo '[ERROR] This tag exists. Use removeTag = true or take another name.'
    }
}