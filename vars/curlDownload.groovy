void call (config, urls, credentials= ''){
  if (config instanceof ArrayList){
         for (int i=0;i<config.size; i++){
             download(config[i],urls,credentials)
        }
    }else {
        download(config,urls,credentials)
    }
}

void download(config, urls, credentials){
    artifactId = config.artifactId
    url = urls[artifactId]
    user = config.user
    destination = config.destination
    if (!config.hosts){
        if (credentials){
            withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: credentials, passwordVariable: 'PASS', usernameVariable: 'USR']]) {
                exec "curl -u \$USR:\$PASS -sS ${url} -o ${destination}"
            }
        }
        else {
            exec "curl -sS ${url} -o ${destination}"
        }
    }else {
        for (int i = 0;i<config.hosts.size;i++) {
            host = config.hosts[i]
            sshagent(credentials:[sshCredentials]) {
                if (credentials){
                    withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: credentials, passwordVariable: 'PASS', usernameVariable: 'USR']]) {
                        exec "curl -u \$USR:\$PASS -sS ${url}|ssh -o StrictHostKeyChecking=no -l ${user} ${host} cat >${destination}"
                    }
                }else {
                    exec "curl -sS ${url}|ssh -o StrictHostKeyChecking=no -l ${user} ${host} cat >${destination}"
                }
            }    
        }
    }
}