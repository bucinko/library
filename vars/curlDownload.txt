Führt einen Download via curl aus.

<h4>Parameter</h4>
<ul>
    <li>
        <b>config</b>
        - Konfiguration
    </li>
    <li>
        <b>urls</b>
        - Key-Value Set von URLs (z.B. aus der Maven uploadToNexus Funktion
    </li>
</ul>

Beispiel - Konfiguration:
<code>
[
    artifactId:"de.deutschepost.testfactory.demoprojekt",
    sshCredentials: "devadminCredentials",
    hosts: ["A9007"],
    user: "devadmin"
    destination:"/vol1/tomcat/webapps"

]
</code>

<h4>Konfigurationsparameter</h4>
<ul>
    <li>
        <b>artifactId</b>
        - Vollständige ArtifactId (GA in maven Koordinaten) muss zum Key in den URLs passen
    </li>
    <li>
        <b>sshCredentials</b>
        - Credentials für Authentifizierung am Remote Host
    </li>
    <li>
        <b>host</b>
        - Liste von Remote Host(s), wenn nicht angegeben, dann wird Localhost genutzt (bzw. der aktive Node)
    </li>
    <li>
        <b>user</b>
        - Benutzer als welcher der Upload durchgeführt werden soll
    </li>
    <li>
        <b>destination</b>
        - Zielverzeichnis auf dem Host
    </li>
</ul>

Werden mehrere Blöcke angegeben, so werden alle Artifakte deployt.
