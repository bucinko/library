/**
 *  Führt ein oder mehrere Kommandos auf einem Remote-Rechner aus.
 *
 * @author David Lorenz (dalo)
 */

void call(String [] commandos, String host, String user, String credentials) {
    sshagent(credentials:[credentials]) {
        for(int i = 0; i < commandos.size(); i++) {
            String commando = commandos[i]
            exec "ssh -l ${user} ${host} $commando"
        }
    }
}
