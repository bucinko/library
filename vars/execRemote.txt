Führt ein oder mehrere Kommandos auf einem Remote-Rechner aus.

<h4>Parameter</h4>
<ul>
    <li>
        <b>commandos</b>
        - Liste von Kommandos, die ausgeführt werden sollen
    </li>
    <li>
        <b>host</b>
        - SSH Host
    </li>
    <li>
        <b>user</b>
        - SSH-Nutzer
    </li>
    <li>
        <b>credentials</b>
        - SSH-Credentials
    </li>
</ul>