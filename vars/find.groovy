List<String> call(String path ='', String include, String exclude ='') {
    if (!path) {
        path = pwd()
    }
    new FileNameFinder().getFileNames(path, include, exclude)
}