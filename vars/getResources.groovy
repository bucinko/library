import hudson.model.Run
import hudson.FilePath

List<String> call(String includes, String excludes = '') {
    List<String> result = []
    Run run = currentBuild.rawBuild
    def action = run.getAction(org.jenkinsci.plugins.workflow.libs.LibrariesAction.class)
    if (action != null) {
        FilePath libs = new FilePath(run.getRootDir()).child("libs")
        for (library in action.getLibraries()) {
            FilePath resources = libs.child("${library.name}/resources/")
            if (resources.exists()) {
                result << resources.list(includes, excludes)
            }
        }
    }
    /* convert the file name to the full file path */
    result.flatten().collect{ it.getRemote() }
}

