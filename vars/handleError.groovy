import dhl.validate.Validate

void call(Map conf, Map replace = [:]) {
    call(null, currentBuild.displayName, conf, replace)
}

void call(e, String buildName, Map conf, Map replace = [:]){
	if (e instanceof org.jenkinsci.plugins.scriptsecurity.sandbox.RejectedAccessException){
		throw e
	} 
    else if(e instanceof org.jenkinsci.plugins.workflow.steps.FlowInterruptedException ){
        echo 'Build was aborted'
        currentBuild.result = 'ABORTED'
    }
    else {
        Validate.notEmpty(conf.body,'Missing body. Set a body in the configuration.')
        Validate.notEmpty(conf.from,'You have to set a e-mail-sender.')
        Validate.notEmpty(conf.to,'You have to set a e-mail-recipient.')
        conf.subject = conf.subject ?: "$buildName failed. Please check!"
        conf.replyTo = conf.replyTo ?: conf.from
        replace['#LINK'] = replace['#LINK'] ?: env.JOB_DISPLAY_URL
        replace['#BUILD_NAME'] = replace['#BUILD_NAME'] ?: buildName
        replace << ["#E": "$e"]

        sendMail(conf, replace)
        currentBuild.result = 'FAILURE'
    }       
}