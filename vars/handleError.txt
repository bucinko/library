A simple way to handle errors.
You have to set following variables:
<ul>
    <li>#E - for the stacktrace and error message</li>
    <li>#BUILD_NAME</li>
</ul>

The function differ between three exceptions:
<ul>
 <li>RejectedAccessException throws the error for the script approval.</li>
 <li>FlowInterruptedException aborts the builds without sending an email.</li>
 <li>All others send an email.</li>
</ul>


<h4>Configurationtemplate</h4>
The simples configuration:
<code>
    [from : "xyz@xyz.com", to:'the@other.com', body: "Something misbehaved. You should have a look there!"]
</code>

The extend version:
<code>
    def conf = [from : "xyz@xyz.com", to:'the@other.com',
        body: "Something misbehaved. You should have a look here: #MYVAR",
        subject : '#BUILD_NAME failed. Help him! Now!', replyTo : 'noreply@other.com',
        ]
    def replace = ['#MYVAR', "This is my var"
</code>