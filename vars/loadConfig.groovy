import dhl.validate.Validate
import dhl.ConfigType
import com.cloudbees.groovy.cps.NonCPS

@NonCPS
Map call(String name, ConfigType type = ConfigType.ALL){
    String filename = Validate.notEmpty(name, "Ein Dateiname muss angegeben werden.")
    LinkedHashMap config = []
    String scriptpath
    String workspace = pwd()
    if (workspace.contains("@")){
        workspace = workspace.substring(0, workspace.lastIndexOf('@'))
    }
    if (!filename.endsWith(".groovy")){
        filename = "${filename}.groovy"
    }

    scriptpath = "${workspace}@script/${filename}"

    File configFile = new File(scriptpath)
    if (!configFile.exists()){
        scriptpath = "${workspace}/${filename}"        
    }

    config = load(scriptpath)
    
    if (type.configPart){
        return config.get(type.configPart)
    }else {
        return config;
    } 
}