void call(Map configuration, Map mailConfig = [:], Map  replace = [:]){
    String credentialsId = configuration.credentialsId
    String folderId = configuration.folderId
    String project = configuration.project
    String localDir = configuration.dir ?: 'app'
    assert folderId

    String projectParam = ''

    if (project){
        projectParam = "-p '${project}'"
    }

    String tfUpload = getResources('teamforgeupload-0.3.4.jar')[0]
    try {
        dir("$localDir/target/sonar"){
            String[] files = find 'TQS_Reports_*.zip'
            if (files == null || files.length != 1) {
                error "[WARNING] More than one or no Reports found for publishing."
            }
            withCredentials([usernamePassword(credentialsId: credentialsId, passwordVariable: 'PASSWORD', usernameVariable: 'USER')]) {
                def filename = new File(files[0]).getName()
                exec("java -jar ${tfUpload} ${projectParam} -H https://lcm.deutschepost.de -U '${env.USER}' -P '${env.PASSWORD}' -m '${env.BUILD_URL} ' -M 'CI/CD-Build'  ${folderId} '${filename}'")
            }
        }
    } catch (e){
        echo "[ERROR] publishing TQS Report failed with the following error: ${e}."
        if (mailConfig) {
            replace << ['#E': "$e"]
            replace << [ '#LINK': currentBuild.rawBuild.absoluteUrl]
            sendMail(mailConfig, replace)
        }
    }
}