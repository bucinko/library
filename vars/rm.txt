Delete a file or directory including subdirectories.

<h4>Parameter</h4>
<ul>
    <li>
        <b>name</b>
        - file or directory name
    </li>
</ul>
