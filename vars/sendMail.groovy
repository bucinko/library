void call(Map conf, Map replace = [:]) {
    for (mailEntry in conf) {
        for (replaceEntry in replace) {
            conf[mailEntry.key] = mailEntry.value.replace(replaceEntry.key, replaceEntry.value)
        }
    }
    mail body: conf.body, charset: 'UTF-8', from: conf.from, replyTo: conf.replyTo, subject: conf.subject, to: conf.to
}
